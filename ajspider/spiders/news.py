# -*- coding: utf-8 -*-
import scrapy

from scrapy.linkextractors import LinkExtractor

from scrapy_splash import SplashRequest


class NewsSpider(scrapy.Spider):
    name = 'news'
    # allowed_domains = ['news.google.com']
    # start_urls = 'https://news.google.com/?hl=zh-TW&gl=TW&ceid=TW:zh-Hant/'

    allowed_domains = ["toscrape.com"]
    start_urls = ['http://quotes.toscrape.com/']
    
    # allowed_domains = ['astro.click108.com.tw']
    # start_urls = ['http://astro.click108.com.tw/']

    # def start_requests(self):
    #     for url in self.start_urls:
    #         # 通过SplashRequest请求等待1秒
    #         yield SplashRequest(url=url, callback=self.parse, args={"wait": 3})

    # def parse(self, response):
    #     print("123")
    #     for url in self.start_urls:
    #         # 通过SplashRequest请求等待1秒
    #         yield SplashRequest(url=url, callback=self.parse, args={"wait": 3})
    #     for element in response.xpath('//div[@class="qx0yFc"]'):
    #         actionUrl = element.xpath('.//a[@class="nuEeue hzdq5d ME7ew"]/@href').extract_first()
            # title = element.xpath('.//a[@class="nuEeue hzdq5d ME7ew"]/text()').extract_first()
    #         source = element.xpath('.//span[@class="IH8C7b Pc0Wt"]/text()').extract_first()
    #         imageUrl = element.xpath('.//img[@class="lmFAjc"]/@src').extract_first()

    #         # item = NewsItem()
    #         # item['title'] = title
    #         # item['image_url'] = imageUrl
    #         # item['action_url'] = actionUrl
    #         # item['source'] = source
    #         print("123")
    #         print(title)
    #         print("456")
    #         # yield item
    #     print("456")

    # def parse(self, response):
    #     le = LinkExtractor()
    #     for link in le.extract_links(response):
    #         yield SplashRequest(
    #             link.url,
    #             self.parse_link,
    #             # endpoint='render.json',
    #             args={
    #                 'har': 1,
    #                 'html': 1,
    #             }
    #         )

    # def parse_link(self, response):
    #     # for element in response.xpath('//div[@class="lBwEZb BL5WZb xP6mwf"]'):
    #         # actionUrl = element.xpath('.//a[@class="nuEeue hzdq5d ME7ew"]/@href').extract_first()
    #         title = element.xpath('//h3[@class="ipQwMb ekueJc gEATFF RD0gLb"]//text()').extract_first()
    #         # source = element.xpath('.//span[@class="IH8C7b Pc0Wt"]/text()').extract_first()
    #         # imageUrl = element.xpath('.//img[@class="lmFAjc"]/@src').extract_first()

    #         print(title)

    def parse(self, response):
        le = LinkExtractor()
        for link in le.extract_links(response):
            yield SplashRequest(
                link.url,
                self.parse_link,
                # endpoint='render.json',
                args={
                    'har': 1,
                    'html': 1,
                }
            )

    # def parse_link(self, response):
    #     print("PARSED", response.real_url, response.url)
    #     # print(response.css("title").extract())
    #     print(response.xpath('//span[@class="text"]/text()').extract_first())
    #     print(response.xpath('//small[@class="author"]/text()').extract_first())
    #     # print(response.xpath(''))
    #     print('===')
    #     # print(response.data["har"]["log"]["pages"])
    #     # print(response.headers.get('Content-Type'))


    # def parse(self, response):
    #     print("123")
    #     for url in self.start_urls:
    #         # 通过SplashRequest请求等待1秒
    #         yield SplashRequest(url=url, callback=self.parse_link, args={"wait": 3})
    #     print("456")

    def parse_link(self, response):
        print("PARSED", response.real_url, response.url)
        # print(response.css("title").extract())
        print(response.xpath('//span[@class="text"]/text()').extract_first())
        print(response.xpath('//small[@class="author"]/text()').extract_first())
        # print(response.xpath(''))
        print('===')
        # print(response.data["har"]["log"]["pages"])
        # print(response.headers.get('Content-Type'))