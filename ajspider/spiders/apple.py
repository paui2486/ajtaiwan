import scrapy 

from scrapy.spiders import CrawlSpider
from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor

#  original class: class AppleCrawler(scrapy.Spider):
#  replace 'scrapy.Spider' with 'CrawlerSpider'
#  CrawSpider: A class is used to crawl multiple websites
class AppleCrawler(CrawlSpider):
    domain = "https://news.pts.org.tw"
    start_urls = ['https://news.pts.org.tw/dailynews.php']
    # rules = [
    #     Rule(LinkExtractor(allow=('/realtimenews/section/new/[1-3]$')), callback='parse_list', follow=True)
    # ]
    name = 'apple'
    def parse(self, response):
        # domain = "https://news.pts.org.tw"
        # res = BeautifulSoup(response.body)
        res = response.xpath('/html/body/section/div/div/div[1]/div[1]/div[1]/div[2]//div[2]/div[1]/a/@href').extract()
        print(res)
        for news in res:
            print (news)
            yield scrapy.Request(news, self.parse_detail)

    def parse_detail(self, response):
        # res = BeautifulSoup(response.body)
        title = response.xpath("//h2[@class='article-title']/text()").extract()
        content = response.xpath("//div[@class='article_content']/text()").extract()

        print(title)
        print(content)
        print('===')
        # appleitem = AppleItem()
        # # define all fields
        # appleitem['title'] = res.select('#h1')[0].text
        # appleitem['content'] = res.select('#summary')[0].text
        # appleitem['time'] = res.select('.gggs time')[0].text
        # return appleitem