# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class AjspiderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # pass
    date = scrapy.Field()
    startname = scrapy.Field()
    allnumber = scrapy.Field()
    alldec = scrapy.Field()
    lovenumber = scrapy.Field()
    lovedec = scrapy.Field()
    causenumber = scrapy.Field()
    causedec = scrapy.Field()
    Fortunenumber = scrapy.Field()
    Fortunedec = scrapy.Field()

    ##news##
class NewsItem(scrapy.Item):
    # 标题
    title = scrapy.Field()
    # 图片的url链接
    image_url = scrapy.Field()
    # 新闻来源
    source = scrapy.Field()
    # 点击的url
    action_url = scrapy.Field()
